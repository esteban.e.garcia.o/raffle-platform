<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Webpatser\Uuid\Uuid;

class CreateRoot extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'root';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create user root';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {             
        $this->create();
    }

    private function create() {
        $this->info('Creating user root');
        $user = new \App\Models\User;
        $user->id = Uuid::generate(4)->string;
        $user->username = $this->ask(__("common.command.user_name"), "root");
        $user->name = $this->ask(__("common.command.name"), "root");                
        $user->password = \Hash::make($this->ask(__("common.command.password"), "$1A2b3c4d"));
        $user->save();
        $this->info("User root created");
    }
}
