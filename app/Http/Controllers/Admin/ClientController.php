<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
class ClientController extends Controller
{     
    public function index($raffle_id) {
        if ($raffle = \App\Models\Raffle::find($raffle_id)) {
            return view('admin.clients.index', compact('raffle'));
        }        
        return  abort(404);
    }

    public function returns($raffle_id) {
        if ($raffle = \App\Models\Raffle::find($raffle_id)) {
            return view('admin.clients.returns', compact('raffle'));
        }        
        return  abort(404);
    }
}
