<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as ControllerMain;
class Controller extends ControllerMain
{

    protected $base = 'admin';

    protected $resourceName = '';

    protected $compacts = []; 

    protected $resourceClass = null;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware(["permission:$this->base.$this->resourceName.read|$this->base.$this->resourceName.write|$this->base.$this->resourceName.update|$this->base.$this->resourceName.delete"]);
    }

    public function index() {
        $this->compactToView();
        extract($this->compacts);
        return view("admin.$this->resourceName.index", \compact(\array_keys($this->compacts)));
    }

    public function create() {
        $this->compactToView();
        extract($this->compacts);
        return view("admin.$this->resourceName.item", \compact(\array_keys($this->compacts)));
    }

    public function edit($id) {
        if ($item = $this->resourceClass::find($id)) {
            $this->compactToView();
            extract($this->compacts);
            return view("admin.$this->resourceName.item", \compact(\array_keys($this->compacts), 'item'));
        }
        return abort(404);
    }

    public function store(Request $request) {
       
        $item = $this->resourceClass::make($request->all());        
        $request->validate(
            $item->rules ? $item->rules : [],
            []
        );
        
        $this->beforeSave($item, $request);
       
        $item->id = \Uuid::generate(4)->string;
        
        if ($item->save()) {
            $this->afterSave($item, $request);
            return redirect()->route("admin.$this->resourceName.index", ["action" => "ok"]);
        } 
        return \abort(500);
    }

    public function update(Request $request, $id) {
        if ($item = $this->resourceClass::find($id)) {
            $this->rulesValidations($item);        
            $request->validate(
                $item->rules ? $item->rules : [],
                []
            );
            $values = $request->all();            
            $this->beforeSave($item, $request, false);            
            if ($item->update($values)) {
                $this->afterSave($item, $request);
                return redirect()->route("admin.$this->resourceName.index", ["action" => "ok"]);
            } 
        }        
        return \abort(400);
    }

    public function show($id) {
        
    }

    protected function compactToView($item = null){}
    

    protected function afterSave($item = null, $request) {}

    protected function beforeSave($item = null, $request, $insert = true) {}

    protected function rulesValidations($item = null){}

    
}
