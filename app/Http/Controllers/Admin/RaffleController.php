<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use ImageResize;
class RaffleController extends Controller
{
    protected $resourceName = 'raffles';
    protected $resourceClass = \App\Models\Raffle::class;

    protected function rulesValidations($item = null){
        if ($item) {
            $item->rules['code'] = 'required|max:5|unique:raffles,code,'.$item->id;
        }
    }

    protected function afterSave($item = null, $request) {
        if ($item && is_null($item->getOriginal('created_by'))) {
            $item->created_by = auth()->id();
            $item->save();
        }
    }

    public function uploadImage(Request $request, $id) {
        if ($raffle = $this->resourceClass::find($id)) {            
            $file = new \App\Models\File;
            $request->validate($file->rules);                                    

            $image = $request->file('file');
            $name = time().'.'.$image->extension();            
            $img = ImageResize::make($image->path());
            $img->resize(400, 400, function ($constraint) {
                $constraint->aspectRatio();
            })->save(public_path('images/raffles') . '/' . $name);                        
            $raffle->images()->create([
                "id" => \Uuid::generate(4)->string,
                "name" => $name
            ]);            
            return redirect()->route('admin.raffles.edit', ['raffle' => $raffle->id]);
        }
        return abort(404);
    }

    public function sellersIndex($id) {
        if ($raffle = $this->resourceClass::find($id)) {   
            $sellers = \App\Models\Seller::withCreator()->get()->map(function ($seller) use($raffle) {
                $sellerPivot = $raffle->sellers()->find($seller->id);
                $seller->is_select = ($sellerPivot ? true : false);
                if ($seller->is_select) {
                    $seller->paid_out = $sellerPivot->pivot->paid_out;
                }                
                return $seller;
            });            
            return view('admin.raffles.sellers.index', compact('sellers', 'raffle'));
        }
        return abort(404);
    }
}
