<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
class SellerController extends Controller
{
    protected $resourceName = 'sellers';
    protected $resourceClass = \App\Models\Seller::class;
    
    protected function afterSave($item = null, $request) {
        if ($item && is_null($item->getOriginal('created_by'))) {
            $item->created_by = auth()->id();
            $item->save();
        }
    }
}
