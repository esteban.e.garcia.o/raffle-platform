<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
class ClientController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {}

    public function datatables($id) {
        if ($raffle = \App\Models\Raffle::find($id)) {
            $items = $raffle->clients;                
            $datatable = DataTables::collection($items);
            $this->columns($datatable);
            $datatable->rawColumns(['actions', 'link']);
            return $datatable->make(true);
        }
        return response()->json(['data' => []]);
    }

    private function columns(&$datatable) {
        $datatable->editColumn('link', function ($item) {
            $url = '';
            if ($link = \App\Models\Link::where('client_id', $item->id)->first()) {
                $url = route('public.raffles.unique.generate.link.client.token', ['id' => $link->id, 'token' => $link->token]);
            }            
            return view('common.span', ['url' => $url, 'text' => 'Ir']);
        });
    }

    public function returnsDatatables($id) {
        if ($raffle = \App\Models\Raffle::find($id)) {
            $items = $raffle->clients()->onlyNotReturned()->get();                
            $datatable = DataTables::collection($items);
            $datatable->editColumn('check', function ($item) {
                return '<input name="' . $item->id .'" id="' . $item->id . '" type="checkbox" class="check-return-ticket">';
            });            
            $datatable->rawColumns(['check']);
            return $datatable->make(true);
        }
        return response()->json(['data' => []]);
    }

    public function returnTicket($id, $client_id) {
        if ($raffle = \App\Models\Raffle::find($id)) {
            if ($client = $raffle->clients()->find($client_id)) {
                $client->is_returned = true;
                $client->save();
                return response()->json(["message" => "ok"]);
            }
            return response()->json(["error" => true, "message" => __('common.errors.error_client_not_found')]);            
        }
        return response()->json(["error" => true, "message" => __('common.errors.error_raffle_not_found')]);
    }
}
