<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
class RaffleController extends Controller
{
    protected $resourceName = 'raffles';

    protected $resourceClass = \App\Models\Raffle::class;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {}

    public function datatables() {
        $items = $this->resourceClass::withCreator()->get();                
        $datatable = DataTables::collection($items);
        $this->columns($datatable);
        $datatable->rawColumns(['actions', 'name']);
        return $datatable->make(true);
    }

    private function columns(&$datatable) {
        $datatable->editColumn('publication', function ($item) {
            return $item->published_at->format("d/m/Y") . ' al ' . $item->closed_at->format("d/m/Y");
        })->editColumn('ticket_price', function ($item) {
            return '$' . $item->ticket_price;
        })->editColumn('tickets_sold', function ($item) {
            return $item->usersCountWithTicket . '  =  $' . number_format($item->usersCountWithTicket * $item->ticket_price);
        })->editColumn('raised', function ($item) {
            return '$' . number_format((($item->usersCountWithTicket * $item->ticket_price) - $item->totalCommission));
        })->editColumn('commission', function ($item) {
            return '$' . number_format($item->totalCommission);
        })->editColumn('name', function ($item) {
            return view('common.span', ['url' => route('public.raffles.show', ['id' => $item->id]), 'text' => $item->name]);
        })->editColumn('actions', function ($item) {
            return view('common.actions', [
                "edit" => true,
                "routeEdit" => route("admin.$this->resourceName.edit", ['raffle' => $item->id]),
                "customs" => [
                    [
                        "url" => route('admin.raffle.clients.index', ['id' => $item->id]),
                        "title" => __('common.clients'),
                        "icon" => "user"
                    ],
                    [
                        "url" => route('admin.raffle.sellers.index', ['id' => $item->id]),
                        "title" => __('common.menu.sellers'),
                        "icon" => "people-carry"
                    ],
                    [
                        "url" => route('admin.raffle.clients.returns', ['id' => $item->id]),
                        "title" => __('common.returns'),
                        "icon" => "exchange-alt"
                    ]  
                ]
            ])->render();
        }); 
    }

    public function generateUniqueLink(Request $request, $code) {
        if ($raffle = $this->resourceClass::published()->where('code', $code)->first()) {
            if ($raffle->usersCountWithTicket < $raffle->total_tickets && $request->has('seller_id')) {
                $link = $raffle->links()->create([
                    "id" => \Uuid::generate(4)->string,
                    "token" => \Str::random(40),
                    "created_by" => auth()->id(),
                    "seller_id" => $request->seller_id                
                ]);
                $url = route('public.raffles.unique.generate.link.client.token', ['id' => $link->id, 'token' => $link->token]);
                return response()->json(["message" => __('common.messages.link_generate_success'), "url" => $url]);
            }
            return response()->json(["error" => true, "message" => __('common.errors.error_limit_total_tickets')]);
        }
        return response()->json(["error" => true, "message" => __('common.errors.raffle_not_found', ['code' => $code])]);
    }

    public function updateWinner($id, $winning_client_id) {
        if ($raffle = $this->resourceClass::find($id)) {
            if ($client = \App\Models\Client::find($winning_client_id)) {
                $raffle->winning_client_id = $client->id;
                $raffle->save();
                return response()->json(["message" => "ok", "client" => $client]);
            }            
            return response()->json(["error" => true, "message" => __('common.errors.error_client_not_found')]);
        }
        return response()->json(["error" => true, "message" => __('common.errors.error_raffle_not_found')]);
    }

    public function addOrDeleteSeller($id, $seller_id, $action) {
        if ($raffle = $this->resourceClass::find($id)) {
            if ($seller = \App\Models\Seller::find($seller_id)) {
                if ($action == 'add') {                    
                    $raffle->sellers()->attach($seller->id, ['id' => \Uuid::generate(4)->string]);
                } else {
                    $raffle->sellers()->detach($seller->id);
                }                
                return response()->json(["message" => "ok"]);
            }         
            return response()->json(["error" => true, "message" => __('common.errors.error_seller_not_found')]);
        }
        return response()->json(["error" => true, "message" => __('common.errors.error_raffle_not_found')]);
    }

    public function sellers($code) {
        if ($raffle = $this->resourceClass::published()->where('code', $code)->first()) {
            $sellers = $raffle->sellers()->orderBy('names')->get();
            return response()->json(["sellers" => $sellers]);      
        }
        return response()->json(["error" => true, "message" => __('common.errors.raffle_not_found', ['code' => $code])]);
    }
}
