<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
class SellerController extends Controller
{
    protected $resourceName = 'sellers';

    protected $resourceClass = \App\Models\Seller::class;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {}

    public function datatables() {
        $items = $this->resourceClass::withCreator()->get();                
        $datatable = DataTables::collection($items);
        $this->columns($datatable);
        $datatable->rawColumns(['actions', 'name']);
        return $datatable->make(true);
    }

    private function columns(&$datatable) {
        $datatable->editColumn('commission', function ($item) {
            return '$' . number_format($item->commission);
        })->editColumn('actions', function ($item) {
            return view('common.actions', [
                "edit" => true,
                "routeEdit" => route("admin.$this->resourceName.edit", ['seller' => $item->id]),
            ])->render();
        }); 
    }

    public function paidOutUpdate(Request $request, $id, $seller_id) {
        if ($raffleSeller = \App\Models\RaffleSeller::where(['raffle_id' => $id, 'seller_id' => $seller_id])->first()) {
            $raffleSeller->paid_out = $request->paid_out;
            $raffleSeller->save();
            return response()->json(["message" => "ok"]);
        }
        return response()->json(["error" => true, "message" => __('common.errors.error_raffle_not_found')]);
    }
}
