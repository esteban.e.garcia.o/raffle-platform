<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;

class ClientController extends Controller
{
    protected $resourceClass = \App\Models\Client::class;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {}

    public function store(Request $request) {
        $client = $this->resourceClass::make($request->all());        
        $request->validate(
            $client->rules ? $client->rules : [],
            []
        );
        if ($request->has('raffle_id') && $raffle = \App\Models\Raffle::find($request->raffle_id)) {
            if ($request->has('link_id') && $link = \App\Models\Link::find($request->link_id)) {  
                $client->id = \Uuid::generate(4)->string;
                if ($client->save()) {
                    if ($raffle->clients()->save($client)) {   
                        $link->client_id = $client->id;
                        $link->save();                     
                        return redirect()->route('public.raffles.unique.generate.link.client.token', ['id' => $link->id, 'token' => $link->token]);
                    }
                }
                return abort(500);              
            }
        }
        return abort(404);
    }

    //Type ajax check
    public function ticketSave(Request $request, $id, $link_id, $client_id) {
        if ($request->ajax()) {
            if ($request->has('ticket')) {
                if ($raffle = \App\Models\Raffle::find($id)) {
                    if ($link = \App\Models\Link::find($link_id)) {
                        if ($raffle->clients()->where('ticket', $request->ticket)->exists()) {
                            return response()->json(['error' => true, 'message' => __('common.errors.error_ticket_exists', ['ticket' => $request->ticket])]);        
                        } else if ($client = $this->resourceClass::find($client_id)) {                             
                            $client->ticket = $request->ticket;
                            $client->save();
                            $url = route('public.raffles.unique.generate.link.client.token', ['id' => $link->id, 'token' => $link->token]);
                            return response()->json(['url' => $url, 'message' => __('common.messages.client.ticket_save_success', ['ticket' => $request->ticket])]);                                                            
                        }
                        return response()->json(['error' => true, 'message' => __('common.errors.error_client_not_found')]);
                    }                    
                }
                return response()->json(['error' => true, 'message' => __('common.errors.error_raffle_not_found')]);
            }
            return response()->json(['error' => true, 'message' => __('common.errors.error_request_ticket')]);
        }
        return response()->json(['error' => true, 'message' => __('common.errors.error_request_ajax')]);
    }

    public function generateVoucherDownload($id, $client_id) {
        if ($raffle = \App\Models\Raffle::find($id)) {
            if ($client = $this->resourceClass::find($client_id)) {
                $signature = base64_encode($client->id . '_' . $client->updated_at);
                $data = [
                    'title' => __('common.platform_name'),
                    'raffle' => $raffle,
                    'client' => $client,
                    'signature' => $signature
                ];
                $pdf = PDF::loadView('templates.generate_voucher', $data);        
                return $pdf->download(__('common.name_pdf', ['ticket' => $client->ticket]) . '.pdf');
            }
        }
        return abort(404);
    }
}
