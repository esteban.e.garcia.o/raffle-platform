<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RaffleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {}

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function generateUniqueLink()
    {        
        return view('raffles.generate_unique_link');
    }

    public function getLinkUniqueClient($id, $token) {
        if ($link = \App\Models\Link::where(['id' => $id, 'token' => $token])->first()) {
            if ($raffle = $link->raffle()->published()->first()) {
                if (is_null($link->client_id)) {
                    return view('raffles.client.register', compact('link', 'raffle'));
                } else {
                    if ($client = $link->client) {
                        if (is_null($client->ticket)) {                            
                            $tickets = $this->getTickets($raffle);                            
                            return view('raffles.client.ticket', compact('raffle', 'link', 'client', 'tickets'));
                        } else {
                            return view('raffles.client.voucher', compact('raffle', 'client'));
                        }
                    }
                }
            }            
        }
        return abort(404);
    }

    private function getTickets($raffle) {
        $numbers = [];
        for ($x = 0; $x < $raffle->total_tickets; $x++)
            $numbers[] = ['id' => ($x + 1)];
        $tickets = collect($numbers);                            
        return $tickets->whereNotIn('id', $raffle->clients()->whereNotNull('ticket')->pluck('ticket'))->values();
    }

    public function show($id) {
        if ($raffle = \App\Models\Raffle::find($id)) {
            return view('raffles.show', compact('raffle'));
        }
        return abort(404);
    }

    public function generateLottery($id) {
        if ($raffle = \App\Models\Raffle::find($id)) {
            $winners = collect();
            $ids = $raffle->clients()->whereNotNull('ticket')->pluck('id');
            $winners = $this->recursiveWinners($winners, $ids);              
            return view('raffles.lottery', compact('raffle', 'winners'));
        }
        return abort(404);
    }

    private function recursiveWinners($winners, $ids) {
        if ($winners->count() === 3) {
            return $winners;
        } else {
            $id = $ids[rand(0, ($ids->count() - 1))];
            if (!$winners->firstWhere('id', $id)) {
                if ($client = \App\Models\Client::find($id)) {
                    $winners->push($client);
                }                
            }
            return $this->recursiveWinners($winners, $ids);
        }
    }
}
