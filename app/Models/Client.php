<?php

namespace App\Models;

class Client extends Model
{
    protected $fillable = [
        'id', 'names', 'surnames', 'phone', 'email', 'address', 'raffle_id', 'ticket'
    ];    

    protected $appends = ['long_name'];

    public $rules = [
        'names' => 'required|max:100',
        'surnames' => 'required|max:100',
        'phone' => 'required|integer|min:10',
        'email' => 'required|email|max:100',
        'address' => 'required',
    ];

    public function getLongNameAttribute() {
        return $this->names . ' ' . $this->surnames;
    }

    public function scopeOnlyReturned($query) {
        return $query->where('is_returned', true);
    }

    public function scopeOnlyNotReturned($query) {
        return $query->where('is_returned', false);
    }
}
