<?php

namespace App\Models;

class File extends Model
{
    protected $fillable = [
        'id', 'name', 'filable_id', 'filable_type'
    ]; 

    public $rules = [
        'file' => 'required|image|mimes:jpeg,png,jpg|max:2048',
    ];

    public function filable()
    {
        return $this->morphTo();
    }
}
