<?php

namespace App\Models;

class Link extends Model
{
    protected $fillable = [
        'id', 'client_id', 'token', 'raffle_id', 'created_by', 'seller_id'
    ];

    public function raffle()
    {
        return $this->belongsTo(Raffle::class);
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function seller()
    {
        return $this->belongsTo(Seller::class);
    }
}
