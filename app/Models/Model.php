<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as ModelBase;

class Model extends ModelBase
{
    public $incrementing = false;

    public function getKeyType()
    {
        return 'string';
    }
}
