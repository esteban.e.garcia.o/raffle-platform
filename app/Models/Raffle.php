<?php

namespace App\Models;

class Raffle extends Model
{
    protected $fillable = [
        'code', 'name', 'published_at', 'closed_at', 'description', 'total_tickets', 'ticket_price', 'created_by'
    ];

    public $rules = [
        'code' => 'required|max:5|unique:raffles,name',
        'name' => 'required',
        'published_at' => 'required',
        'closed_at' => 'required',
        'description' => 'required',
        'total_tickets' => 'required|integer',
        'ticket_price' => 'required|integer'
    ];

    protected $casts = [
        'published_at' => 'date',
        'closed_at' => 'date'
    ];

    public function clients() {
        return $this->hasMany(Client::class);
    }

    public function links() {
        return $this->hasMany(Link::class);
    }

    public function scopePublished($query) {
        return $query->where('published_at', '<=', now())->where('closed_at', '>=', now());
    }

    public function images() {
        return $this->morphMany(File::class, 'filable');
    }

    public function getUsersCountWithTicketAttribute() {
        return $this->clients()->whereNotNull('ticket')->count();
    }

    public function scopeWithCreator($query) {
        return $query->where('created_by', auth()->id())->orWhere('created_by', null);
    }

    public function sellers()
    {
        return $this->belongsToMany(Seller::class)->withPivot('id', 'paid_out');
    }

    public function getTotalCommissionAttribute() {
        $sum = 0;
        $links = $this->links()->whereNotNull('client_id')->whereNotNull('seller_id')->get();
        foreach ($links as $link) {
            $sum += $link->seller->commission;
        }        
        return $sum;
    }
}
