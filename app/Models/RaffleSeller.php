<?php

namespace App\Models;

class RaffleSeller extends Model
{
    protected $table = 'raffle_seller';
    protected $fillable = [
        'id', 'paid_out'
    ];
}
