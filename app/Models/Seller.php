<?php

namespace App\Models;

class Seller extends Model
{
    protected $fillable = [
        'names', 'commission'
    ];

    public $rules = [
        'names' => 'required|max:200',        
        'commission' => 'required|integer'
    ];

    public function scopeWithCreator($query) {
        return $query->where('created_by', auth()->id());
    }

    public function links() {
        return $this->hasMany(Link::class);
    }

    public function getTotalCommissionAttribute() {        
        $count = $this->links()->whereNotNull('client_id')->count();              
        return ($count * $this->commission);
    }
}
