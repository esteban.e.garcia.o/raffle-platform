const url_raffles_seller_add_or_delete = $("meta[name=url-raffles-seller-add-or-delete]").attr('content');
const url_raffle_seller_paid_out_update = $("meta[name=url-raffle-seller-paid-out-update]").attr('content');
$(() => {
    $(document).on('click', '.check-select-seller', e => {
        e = e.currentTarget;
        let seller_id = $(e).data('obj').id;
        let action = ($(e).prop('checked') ? 'add' : 'delete');
        saveSellerSelect(seller_id, action);
    });

    $(document).on('focusout', '.input-number-paid-out', e => {
        e = e.currentTarget;
        let obj = $(e).data('obj');
        let paid_out = $(e).val();
        if (paid_out !== "") {
            window.sendRequest({
                method: 'POST',
                url: url_raffle_seller_paid_out_update.replace(':seller_id', obj.id),        
                data: {paid_out},
                success: (response) => {
                    if (response.message == 'ok') window.location.reload();
                },
                error: (e) => {
                    console.log("Error", e);            
                }
            }, {processData: true});
        }
    });
});

const saveSellerSelect = (seller_id, action) => {
    window.sendRequest({
        method: 'POST',
        url: url_raffles_seller_add_or_delete.replace(':seller_id', seller_id).replace(':action', action),        
        success: (response) => {},
        error: (e) => {
            console.log("Error", e);            
        }
    }, {processData: true});
}