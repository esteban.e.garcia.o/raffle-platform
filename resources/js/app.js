require('./bootstrap');
import Swal from 'sweetalert2';
import assign from 'lodash-es/assign';
const translations = JSON.parse($("meta[name=translation_common]").attr('content'));

(function($){
    if (typeof sendRequest === "undefined") {
      window.sendRequest = function(data, options = {}){
        var method = data.method ? data.method : 'POST';      
        const config = {
          method,
          url: data.url,
          dataType: 'json',
          data: data.data,
          processData: false,
          headers: {"Authorization": "Bearer " + $('meta[name=api-token]').attr('content')},
          success: data.success,
          beforeSend: data.beforeSend,
          complete: data.complete,
          error: function(xhr, status, error){
            //Callback from fail
            if(data.error){data.error(xhr);}
          }
        }
        return $.ajax(assign(config, options));
      };
    }
  })($);
  
window.getMessage = (title, icon = 'error', callback = null) => {
    Swal.fire({
        icon,
        title,
        text: '',
    }).then(callback);
}

window.getConfirm = (title, callback) => {
  Swal.fire({
    title,
    text: "",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: translations.accept,
    cancelButtonText: translations.cancel
  }).then((result) => {
    if (result.isConfirmed && callback) {
      callback();      
    }
  });
}

window.loading = () => {
  Swal.fire({
    title: translations.waiting,
    html: '',
    timer: false,
    timerProgressBar: false,
    onBeforeOpen: () => {
      Swal.showLoading();
    },
    onClose: () => {
      
    }
  }).then((result) => {
    /* Read more about handling dismissals below */
    if (result.dismiss === Swal.DismissReason.timer) {
      console.log('I was closed by the timer')
    }
  });
}