const tickets = ($("meta[name=tickets]").length > 0 ? JSON.parse($("meta[name=tickets]").attr('content')) : 0);
const translations = JSON.parse($("meta[name=translation_common]").attr('content'));
const url_raffle_link_client_ticket = $("meta[name=url-raffle-link-client-ticket]").attr('content');
const _token = $("meta[name=csrf-token]").attr('content');
var index = 0;
var limit = 30;
var total = 30;
var remaining = 0;
$(() => {
    if (total >= tickets.length) {
        $(".btn-tickets-right").addClass('disabled');
    }
    buildTableTickets();
    $(document).on('click', '.box-ticket', e => {
        e = e.currentTarget;
        $("*").removeClass('select-ticket');
        $(e).addClass('select-ticket');
        $(".btn-ticket-send").removeClass('disabled');
    });

    $(document).on('click', '.btn-tickets-left', e => {
        e = e.currentTarget;
        $(".btn-tickets-right").removeClass('disabled');
        $(".btn-ticket-send").addClass('disabled');
        if ((total - limit) === limit) {
            $(e).addClass('disabled');
            total = total - limit;
            index = 0;            
        } else {
            if (remaining !== 0) {
                total = total - remaining;
                index = index - (remaining + limit);                 
                remaining = 0;
            } else {
                total = total - limit;
                index = index - (limit * 2);
            }                                    
        }
        buildTableTickets();
    });

    $(document).on('click', '.btn-tickets-right', e => {
        e = e.currentTarget;
        $(".btn-tickets-left").removeClass('disabled');
        $(".btn-ticket-send").addClass('disabled');
        if ((total + limit) > tickets.length) {
            $(e).addClass('disabled');
            remaining = (tickets.length - total);
            total = total + remaining;            
        } else {
            total = total + limit;            
        }
        buildTableTickets();
    });

    $(document).on('click', '.btn-ticket-send', e => {
        e = e.currentTarget;
        let ticket = $("div.select-ticket[data-ticket]").data('ticket');
        if (ticket) {
            window.getConfirm(translations.messages.client.warning_send_ticket.replace(':ticket', ticket), () => {
                window.loading();
                window.sendRequest({
                    method: 'POST',
                    url: url_raffle_link_client_ticket,    
                    data: {_token, ticket},    
                    success: (response) => {
                        if (response.error)
                            window.getMessage(response.message);
                        else if (response.url){         
                            window.getMessage(response.message, 'success', () => {
                                window.location.href = response.url;
                            });
                        }
                            
                    },
                    error: (e) => {
                        console.log("Error", e);            
                    }
                }, {processData: true});
            });
        } else {
            window.getMessage(translations.errors.error_not_select_ticket);
        }
    });
});

const buildTableTickets = () => {
    $("#content-raffle-tickets").html('');
    for (index; index < total; index++) {        
        if (tickets[index]) {
            let ticket = tickets[index]['id'];
            $("#content-raffle-tickets").append(`<div class="col-md-2 box-ticket" data-ticket="${ticket}">${ticket}</div>`);
        } else 
            break;     
    }
}