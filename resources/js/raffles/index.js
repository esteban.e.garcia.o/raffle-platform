const url_raffles_generate_unique_link = $("meta[name=url-raffles-generate-unique-link]").attr('content');
const url_raffles_sellers = $("meta[name=url-raffles-sellers]").attr('content');
const translations = JSON.parse($("meta[name=translation_common]").attr('content'));
$(() => {
    $("#btn-generate-unique-link").on('click', e => {
        e = e.currentTarget;        
        let code = $("#code").val(), seller_id = $("#select-seller").val();
        if (code !== "" && code.length == 5 && seller_id) {            
            generateUniqueLink(code, seller_id);
        } else {
            window.getMessage(translations.errors.error_code);
        }
    });
    $("#btn-copy-text").on('click', () => {        
        let copy = document.getElementById("copy");
        copy.select();         
        document.execCommand('copy');
        window.getMessage(translations.text_copy, 'success');
    });
    $("#code").on('focusout', e => {
        e = e.currentTarget;
        let code = $("#code").val();
        if (code !== "" && code.length == 5) {            
            getSellers(code);
        } else {
            window.getMessage(translations.errors.error_code);
        }
    });
});

const generateUniqueLink = (code, seller_id) => {
    window.sendRequest({
        method: 'POST',
        url: url_raffles_generate_unique_link.replace(':code', code),        
        data: {seller_id},
        success: (response) => {
            if (response.error)
                window.getMessage(response.message);
            else {         
                $("#label-generate-unique-link").show();       
                $("#label-generate-unique-link").find('textarea').val('');
                $("#label-generate-unique-link").find('textarea').val(`${response.message}\n${response.url}`);
            }
                
        },
        error: (e) => {
            console.log("Error", e);            
        }
    }, {processData: true});
}

const getSellers = (code) => {
    window.sendRequest({
        method: 'POST',
        url: url_raffles_sellers.replace(':code', code),        
        success: (response) => {
            if (response.sellers && response.sellers.length > 0) {
                $("#select-seller").html('');
                $.each(response.sellers, (key, seller) => {
                    $("#select-seller").append(`<option value="${seller.id}">${seller.names}</option>`);
                });   
                $("#btn-generate-unique-link").removeClass('disabled');
            }            
        },
        error: (e) => {
            console.log("Error", e);            
        }
    }, {processData: true});
}