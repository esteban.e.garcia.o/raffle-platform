var winners = JSON.parse($("meta[name=winners]").attr('content'));
const translations = JSON.parse($("meta[name=translation_common]").attr('content'));
const url_raffle_winner_update = $("meta[name=url-raffle-winner-update]").attr('content');
$(() => {    
    $(".btn-start-lottery").on('click', e => {        
        countingLottery();
    });
    $(".btn-stir-lottery").on('click', e => {
        e = e.currentTarget;
        stirLottery();
    });  
    $(".card-winner").on('click', e => {
        e = e.currentTarget;
        let id = $(e).data('id');
        if (id) {
            let winner = winners.find(w => w.id == id);
            if (winner) {
                $(e).hide(400, () => {
                    $(e).attr('style', `font-size: ${20/winner.ticket.toString().length}rem;`)
                    if ($(e).hasClass('winner')) {
                        $(e).addClass('winner-unique');
                        $(e).text(winner.ticket);
                        sendWinner(id);
                    } else {
                        $(e).text(winner.ticket);
                    }
                });                
                $(e).show(400);
            }
        }        
    });    
});

const countingLottery = () => {
    var text = [4, 3, 2, 1];
    var counter = 0;
    var elem = $("#panel-circle");
    let interval = setInterval(() => {
        elem.fadeOut(function(){
            elem.html(text[counter]);
            counter++;
            if(counter >= text.length) {
                elem.hide(); 
                $(".btn-start-lottery").hide();
                $("#panel-winners-list").show(300);
                $(".btn-stir-lottery").show();
                clearInterval(interval);                                
            } else
                elem.fadeIn();
        });
    }, 1500);
}

const stirLottery = () => {    
    var random = randomNumber(0, 3);
    var counter = 0;
    var elem = $(".card-winner");
    let interval = setInterval(() => {
        $(elem[random]).fadeOut(function(){   
            counter++;  
            if (counter === 12) {
                checkWinners();
                clearInterval(interval);  
            }                         
            $(elem[random]).fadeIn();               
            random = randomNumber(0, 3);         
        });
    }, 900);
}

const randomNumber = (min, max) => { 
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min) + min);
}

const checkWinners = () => {
    winners = winners.sort((a,b) => {
        return (Math.random()-0.5);
    });
    $("#panel-winners-list").find('div.card-winner').each((key, item) => {
        if (winners[key]) {
            $(item).data('id', winners[key]['id']);
        }
        if (key == 1) {
            $(item).addClass('winner');
        }
    });
}

const sendWinner = (id) => {
    window.sendRequest({
        method: 'POST',
        url: url_raffle_winner_update.replace(':winning_client_id', id),        
        success: (response) => {
            if (response.error)
                window.getMessage(response.message);
            else if (response.client){   
                let client = response.client;      
                $("#winner-name").append(`<b>${translations.congratulations}</b> ${client.long_name}`);
                $("#winner-phone").text(translations.inputs.phone + ': ' + client.phone);
                $("#panel-winners-information").show();
            }
        },
        error: (e) => {
            console.log("Error", e);            
        }
    }, {processData: true});
}