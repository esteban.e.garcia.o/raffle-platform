@extends('layouts.app')

@section('content')
    @parent
    <x-content>
        <x-slot name="title">@lang('common.administration', ['model' => __('common.clients')])</x-slot>
        <div class="row py-4">
            <div class="col-md-12">                
                <a href="{{ route('admin.raffles.index') }}" class="btn btn-outline-dark"><i class="fas fa-long-arrow-alt-left"></i> @lang('common.cancel')</a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @include('common.table', [
                    "resource" => "clients",
                    "headers" => [
                        "names" => __('common.inputs.names'),
                        "surnames" => __('common.inputs.surnames'),
                        "email" => __('common.inputs.email'),
                        "phone" => __('common.inputs.phone'),
                        "ticket" => __('common.inputs.ticket'),
                        "link" => __('common.link'),
                    ],
                    "url" => route('api.raffles.clients.datatables', ['id' => $raffle->id])
                ])
            </div>
        </div>
    </x-content>
@endsection