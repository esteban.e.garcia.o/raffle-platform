@extends('layouts.app')

@section('content')
    @parent
    <x-content>
        <x-slot name="title">@lang('common.administration', ['model' => __('common.returns')])</x-slot>
        <div class="row py-4">
            <div class="col-md-12">                
                <a href="{{ route('admin.raffles.index') }}" class="btn btn-outline-dark"><i class="fas fa-long-arrow-alt-left"></i> @lang('common.cancel')</a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @include('common.table', [
                    "resource" => "clients",
                    "headers" => [
                        "names" => __('common.inputs.names'),
                        "surnames" => __('common.inputs.surnames'),
                        "email" => __('common.inputs.email'),
                        "phone" => __('common.inputs.phone'),
                        "ticket" => __('common.inputs.ticket'),
                        "check" => __('common.return'),
                    ],
                    "url" => route('api.raffles.clients.returns.datatables', ['id' => $raffle->id])
                ])
            </div>
        </div>
    </x-content>
@endsection
@push('metas')
    <meta name="url_client_return_ticket" content="{{ route('api.raffles.client.return.ticket', [$raffle->id, ':client_id']) }}">
@endpush
@push('scripts')
    <script>
        const url_client_return_ticket = $("meta[name=url_client_return_ticket]").attr('content');
        $(() => {
            $(document).on('click', '.check-return-ticket', e => {
                e = e.currentTarget;
                if ($(e).prop('checked')) {
                    let client_id = $(e).attr('name');
                    window.sendRequest({
                        method: 'POST',
                        url: url_client_return_ticket.replace(':client_id', client_id),                            
                        success: (response) => {
                            if (response.message == 'ok') window.location.reload();
                        },
                        error: (e) => {
                            console.log("Error", e);            
                        }
                    }, {processData: true});
                }
            });
        });
    </script>
@endpush