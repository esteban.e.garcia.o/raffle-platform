@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
  <div class="col-md-12">
    {{--<a class="float-right" href="{{ asset('app/inventory-app.apk') }}" target="_blank">Descargar App</a>--}}
  </div>
  <div class="col-md-6 ">
    <h1>@lang('common.welcome_home')</h1>
    <br><br><br>
    <a href="{{ route('raffles.generate.unique.link.client') }}" class="btn btn-outline-primary"><i class="fas fa-plus"></i> @lang('common.generate_unique_link')</a>                
  </div>
  <div class="col-md-6">
    <h1>@lang('common.raffle_published')</h1>
    <br><br><br>
    <ul class="list-group">
      @foreach ($raffles as $raffle)
        <li class="list-group-item d-flex justify-content-between align-items-center">
          <a href="{{ route('raffles.generate.lottery', ['id' => $raffle->id]) }}">{{ $raffle->name }}</a>
          <span class="badge badge-primary badge-pill">@lang('common.tickets_sold'): {{ $raffle->usersCountWithTicket }}</span>
        </li>
      @endforeach      
    </ul>
  </div>
</div>
@endsection