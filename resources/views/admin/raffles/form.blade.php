<div class="row">
    @include('common.input', [
        "id" => "code",
        "required" => true,
        "clas" => "col-md-6",
        "label" => __('common.inputs.code'),
        "value" => (isset($item->code) ? $item->code : old('code')),
        "maxLength" => "5"
    ])
    @include('common.input', [
        "id" => "name",
        "required" => true,
        "clas" => "col-md-6",
        "label" => __('common.inputs.name'),
        "value" => (isset($item->name) ? $item->name : old('name'))
    ])
</div>
<div class="row">
    @include('common.input', [
        "id" => "published_at",
        "required" => true,
        "clas" => "col-md-6",
        "label" => __('common.inputs.published_at'),
        "value" => (isset($item->published_at) ? $item->published_at->format("Y-m-d") : old('published_at')),
        "type" => "date"
    ])
    @include('common.input', [
        "id" => "closed_at",
        "required" => true,
        "clas" => "col-md-6",
        "label" => __('common.inputs.closed_at'),
        "value" => (isset($item->closed_at) ? $item->closed_at->format("Y-m-d") : old('closed_at')),
        "type" => "date"
    ])
</div>
<div class="row">
    @include('common.textarea', [
        "id" => "description",
        "required" => true,
        "clas" => "col-md-12",
        "label" => __('common.inputs.description'),
        "value" => (isset($item->description) ? $item->description : old('description')),        
    ])
</div>
<div class="row">
    @include('common.input', [
        "id" => "total_tickets",
        "required" => true,
        "clas" => "col-md-6",
        "label" => __('common.inputs.total_tickets'),
        "value" => (isset($item->total_tickets) ? $item->total_tickets : old('total_tickets')),
        "type" => "number"
    ])
    @include('common.input', [
        "id" => "ticket_price",
        "required" => true,
        "clas" => "col-md-6",
        "label" => __('common.inputs.ticket_price'),
        "value" => (isset($item->ticket_price) ? $item->ticket_price : old('ticket_price')),
        "type" => "number"
    ])
</div>
@push('scripts')
    <script src="{{ asset('js/lib/ckeditor/ckeditor.js') }}"></script>
    <script>
        CKEDITOR.replace( 'description' );
    </script>
@endpush