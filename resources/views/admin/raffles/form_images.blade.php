<x-content>
    <x-slot name="title">@lang('common.add') @lang('common.images')</x-slot>
    <div class="row py-4">
        <div class="col-md-12">
            @component('components.form', [
                "item" => null,
                "resource" => "raffles-images",                    
                "route" => route('admin.raffle.upload.image', ['id' => $item->id])
            ])            
            <div class="row">
                @include('common.input', [
                    "id" => "file",
                    "required" => true,
                    "clas" => "col-md-6",
                    "label" => __('common.inputs.image'),
                    "value" => '',
                    "type" => "file",
                    "accept" => "image/png, image/jpeg"
                ])
                @if ($item->images->count() > 0)                
                    <div class="col-md-6">
                        <label>Lista de imagenes</label>
                        <ul class="list-group">
                            @foreach ($item->images as $image)
                                <li class="list-group-item">
                                    <a href="{{ asset('images/raffles/' . $image->name) }}" target="_blank">{{ $image->name }}</a>
                                    
                                </li>                            
                            @endforeach                            
                        </ul>
                    </div>                
                @endif
            </div>                 
            @endcomponent 
        </div>
    </div>        
</x-content>