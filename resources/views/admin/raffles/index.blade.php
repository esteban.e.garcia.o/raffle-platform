@extends('layouts.app')

@section('content')
    @parent
    <x-content>
        <x-slot name="title">@lang('common.administration', ['model' => __('common.raffles')])</x-slot>
        <div class="row py-4">
            <div class="col-md-12">
                <a href="{{ route('admin.raffles.create') }}" class="btn btn-outline-primary float-right"><i class="fas fa-plus"></i> @lang('common.add')</a>
                <a href="{{ route('home') }}" class="btn btn-outline-dark"><i class="fas fa-long-arrow-alt-left"></i> @lang('common.cancel')</a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @include('common.table', [
                    "resource" => "raffles",
                    "headers" => [
                        "code" => __('common.inputs.code'),
                        "name" => __('common.inputs.name'),
                        "publication" => __('common.inputs.availability'),
                        "total_tickets" => __('common.inputs.total_tickets'),
                        "ticket_price" => __('common.inputs.ticket_price'),
                        "tickets_sold" => __('common.tickets_sold'),
                        "raised" => __('common.raised'),
                        "commission" => __('common.inputs.commission'),
                        "actions" => __('common.inputs.actions'),
                    ],
                    "url" => route('api.raffles.datatables')
                ])
            </div>
        </div>
    </x-content>
@endsection