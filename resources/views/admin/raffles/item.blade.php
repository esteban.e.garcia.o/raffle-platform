@extends('layouts.app')

@section('content')
    @parent
    <x-content>
        <x-slot name="title">@lang('common.' . (isset($item) ? 'edit' : 'add')) @lang('common.raffle')</x-slot>
        <div class="row py-4">
            <div class="col-md-12">
                @component('components.form', [
                    "item" => (isset($item) ? $item : null),
                    "resource" => "raffles",
                    "back" => route('admin.raffles.index'),
                    "updateResource" => "raffle"
                ])
                    @include('admin.raffles.form', ['item' => (isset($item) ? $item : null)])
                @endcomponent
            </div>
        </div>        
    </x-content>
    @isset($item)
        <br>
        @include('admin.raffles.form_images', ['item' => $item])
    @endisset
@endsection