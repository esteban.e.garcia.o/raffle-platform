@extends('layouts.app')

@section('content')
    @parent
    <x-content>
        <x-slot name="title">{{ $raffle->name }} - @lang('common.menu.sellers')</x-slot>
        <div class="row py-4">
            <div class="col-md-12">                
                <a href="{{ route('admin.raffles.index') }}" class="btn btn-outline-dark"><i class="fas fa-long-arrow-alt-left"></i> @lang('common.cancel')</a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-striped">
                    <thead class="thead-dark">
                      <tr>
                        <th scope="col">@lang('common.select')</th>
                        <th scope="col">@lang('common.inputs.full_name')</th>
                        <th scope="col">@lang('common.inputs.commission')</th>
                        <th scope="col">@lang('common.total')</th>
                        <th scope="col">@lang('common.paid_out')</th>
                        <th scope="col">@lang('common.pending_pay')</th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($sellers as $seller)
                            <tr>
                                <td><input @if($seller->is_select) checked @endif type="checkbox" class="check-select-seller" name="select" id="select" data-obj="{{ $seller->toJson() }}"></td>
                                <td>{{ $seller->names }}</td>
                                <td>${{ $seller->commission }}</td>
                                <td>
                                    @if ($seller->commission)
                                        ${{ number_format($seller->totalCommission) }}
                                    @else
                                        $0
                                    @endif
                                </td>
                                <td style="width: 150px;">
                                    @if ($seller->commission > 0)
                                        <input class="form-control input-number-paid-out" value="{{ $seller->paid_out }}" type="number" name="paid_out" id="paid_out" data-obj="{{ $seller->toJson() }}">
                                    @endif
                                </td>
                                <td>
                                    @if ($seller->commission > 0)
                                        ${{ number_format($seller->totalCommission - $seller->paid_out) }}
                                    @else
                                        $0
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </x-content>
@endsection
@push('metas')
    <meta name="url-raffles-seller-add-or-delete" content="{{ route('api.raffles.seller.add.or.delete', [$raffle->id, ':seller_id', ':action']) }}">    
    <meta name="url-raffle-seller-paid-out-update" content="{{ route('api.raffle.seller.paid_out.update', [$raffle->id, ':seller_id']) }}">        
@endpush
@push('scripts')
    <script src="{{ asset('js/build/admin/raffles/sellers.js') }}?v=1" defer></script>   
@endpush