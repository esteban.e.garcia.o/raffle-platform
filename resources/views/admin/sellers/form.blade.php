<div class="row">
    @include('common.input', [
        "id" => "names",
        "required" => true,
        "clas" => "col-md-6",
        "label" => __('common.inputs.full_name'),
        "value" => (isset($item->names) ? $item->names : old('names'))
    ])
    @include('common.input', [
        "id" => "commission",
        "required" => true,
        "clas" => "col-md-6",
        "label" => __('common.inputs.commission'),
        "value" => (isset($item->commission) ? $item->commission : old('commission')),
        "type" => "number"
    ])
</div>