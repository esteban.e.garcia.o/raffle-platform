@extends('layouts.app')

@section('content')
    @parent
    <x-content>
        <x-slot name="title">@lang('common.administration', ['model' => __('common.menu.sellers')])</x-slot>
        <div class="row py-4">
            <div class="col-md-12">
                <a href="{{ route('admin.sellers.create') }}" class="btn btn-outline-primary float-right"><i class="fas fa-plus"></i> @lang('common.add')</a>
                <a href="{{ route('home') }}" class="btn btn-outline-dark"><i class="fas fa-long-arrow-alt-left"></i> @lang('common.cancel')</a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @include('common.table', [
                    "resource" => "sellers",
                    "headers" => [
                        "names" => __('common.inputs.full_name'),
                        "commission" => __('common.inputs.commission'),
                        "actions" => __('common.inputs.actions'),
                    ],
                    "url" => route('api.sellers.datatables')
                ])
            </div>
        </div>
    </x-content>
@endsection