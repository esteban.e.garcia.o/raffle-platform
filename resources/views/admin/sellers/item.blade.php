@extends('layouts.app')

@section('content')
    @parent
    <x-content>
        <x-slot name="title">@lang('common.' . (isset($item) ? 'edit' : 'add')) @lang('common.seller')</x-slot>
        <div class="row py-4">
            <div class="col-md-12">
                @component('components.form', [
                    "item" => (isset($item) ? $item : null),
                    "resource" => "sellers",
                    "back" => route('admin.sellers.index'),
                    "updateResource" => "seller"
                ])
                    @include('admin.sellers.form', ['item' => (isset($item) ? $item : null)])
                @endcomponent
            </div>
        </div>        
    </x-content>
@endsection