@if (isset($edit) && $edit == true)
    <a href="{{ $routeEdit }}" class="btn btn-outline-dark" data-toggle="tooltip" data-placement="top" title="@lang('common.edit')"><i class="fas fa-pencil-alt"></i></a>
@endif
@if (isset($see) && $see == true)
    <button data-items="{{ isset($items) ? $items->toJson(): '[]' }}" onclick="{{ $seeEvent }}" class="btn btn-outline-dark" data-toggle="tooltip" data-placement="top" title="{{ $title }}"><i class="far fa-eye"></i></button>
@endif
@isset($customs)
    @foreach ($customs as $custom)
        <a href="{{ $custom['url'] }}" class="btn btn-outline-dark" data-toggle="tooltip" data-placement="top" title="{{ $custom['title'] }}"><i class="fas fa-{{ $custom['icon'] }}"></i></a>
    @endforeach
@endisset