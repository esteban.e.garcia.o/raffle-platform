<div class="{{ isset($clas) ? $clas : 'col-md-12' }}">
    <div class="form-group">
        @isset($label)
            <label for="{{ $id }}">{{ $label }}</label>
        @endisset
        <input 
            type="{{ isset($type) ? $type : 'text' }}" 
            class="form-control @error($id) is-invalid @enderror" 
            id="{{ $id }}" 
            name="{{ $id }}" 
            @if (isset($required) && $required == true) required @endif
            placeholder="@isset($placeholder) {{ $placeholder }} @else {{ isset($label) ? $label : '' }} @endisset"
            value="{{ $value }}"
            @isset($maxLength) maxlength="{{$maxLength}}" @endisset
            @if (isset($type) && $type == "number") step="0.01" @endif
            @if (isset($type) && $type == "tel") pattern="[0-9]{10}" @endif
            @if (isset($type) && $type == 'file' && isset($accept)) accept="{{ $accept }}" @endif
            >
        @isset($warningText)
            <i class="orange" style="font-size: 12px">{{ $warningText }}</i>
        @endisset
        @error($id)
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>