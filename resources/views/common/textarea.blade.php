<div class="{{ isset($clas) ? $clas : 'col-md-12' }}">
    <div class="form-group">
        @isset($label)
            <label for="{{ $id }}">{{ $label }}</label>
        @endisset
        <textarea             
            class="form-control @error($id) is-invalid @enderror" 
            style="height: 120px;"
            id="{{ $id }}" 
            name="{{ $id }}" 
            @if (isset($required) && $required == true) required @endif
            placeholder="@isset($placeholder) {{ $placeholder }} @else {{ isset($label) ? $label : '' }} @endisset"                        
            @isset($disabled) disabled @endisset
            >{{ $value }}</textarea>
        @error($id)
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>