<div id="carousel-items" class="carousel slide" data-ride="carousel" data-interval="5000">
    <ol class="carousel-indicators">
        @for ($x = 0; $x < sizeof($images); $x++)
            <li data-target="#carousel-items" data-slide-to="{{ $x }}" @if($x == 0) class="active" @endif></li>      
        @endfor
    </ol>
    <div class="carousel-inner">
        @foreach ($images as $key => $image)
            <div class="carousel-item @if($key == 0) active @endif">
                <img class="d-block w-100" src="{{ asset('images/raffles/' . $image->name) }}" alt="{{ $image->name }}">
            </div>
        @endforeach 
    </div>
    <a class="carousel-control-prev" href="#carousel-items" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">@lang('common.previous')</span>
    </a>
    <a class="carousel-control-next" href="#carousel-items" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">@lang('common.next')</span>
    </a>
</div>