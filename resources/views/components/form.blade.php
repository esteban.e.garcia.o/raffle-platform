<form action="{{ (isset($route) ? $route : (is_null($item) ? route('admin.' . $resource . '.store') : route('admin.' . $resource . '.update', [$updateResource => $item->id]))) }}" method="POST" id="form-{{ $resource }}-actions" enctype="multipart/form-data">
    @csrf
    @if (!is_null($item))
        {{ method_field('PUT') }}
    @endif
    {{ $slot }}    
    <div class="form-group">
        @isset($back)
            <button type="submit" class="btn btn-outline-primary float-right"><i class="fas fa-save"></i> @lang('common.save')</button>
            <a href="{{ $back }}" class="btn btn-outline-dark"><i class="fas fa-long-arrow-alt-left"></i> @lang('common.cancel')</a>
        @else
            <button type="submit" class="btn btn-outline-primary"><i class="fas fa-save"></i> @lang('common.save')</button>
        @endisset
    </div>
</form>