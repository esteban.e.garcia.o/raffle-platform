<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">    
    <meta name="translation_common" content="{{ json_encode(__('common')) }}">
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" />

    <title>@lang('common.platform_name')</title>
    

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css"/> 

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}?v=1" rel="stylesheet">
    @stack('metas')
</head>
<body>
    <div id="app">
        @include('layouts.main_menu', ['only_name_platform' => true])
        <main class="py-4">
            <div class="container">
                @yield('content')
            </div>
        </main>
    </div>
    <!-- Scripts -->    
    <!-- Jquery -->
    <script src="{{ asset('js/build/app.js') }}" defer></script>      
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>        
    @stack('scripts')
</body>
</html>