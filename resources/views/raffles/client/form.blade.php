<div class="row">
    @include('common.input', [
        "id" => "names",
        "required" => true,
        "clas" => "col-md-6",
        "label" => __('common.inputs.names'),
        "value" => old('names'),
        "maxLength" => "100"
    ])
    @include('common.input', [
        "id" => "surnames",
        "required" => true,
        "clas" => "col-md-6",
        "label" => __('common.inputs.surnames'),
        "value" => old('surnames'),
        "maxLength" => "100"
    ])
</div>
<div class="row">
    @include('common.input', [
        "id" => "phone",
        "required" => true,
        "clas" => "col-md-6",
        "label" => __('common.inputs.phone'),
        "value" => old('phone'),
        "maxLength" => "10",
        "type" => "tel",
        "warningText" => __('common.messages.client.warning_phone')
    ])
    @include('common.input', [
        "id" => "email",
        "required" => true,
        "clas" => "col-md-6",
        "label" => __('common.inputs.email'),
        "value" => old('email'),
        "maxLength" => "100",
        "type" => "email",
        "warningText" => __('common.messages.client.warning_email')
    ])
</div>
<div class="row">
    @include('common.input', [
        "id" => "address",
        "required" => true,
        "clas" => "col-md-12",
        "label" => __('common.inputs.address'),
        "value" => old('address'),
        "warningText" => __('common.messages.client.warning_address')
    ])
</div>
<input type="hidden" name="raffle_id" id="raffle_id" value="{{ $raffle->id }}">
<input type="hidden" name="link_id" id="link_id" value="{{ $link->id }}">