@extends('layouts.app_public')

@section('content')
    @parent
    @include('raffles.info_raffle', ['raffle' => $raffle])
    <div class="row">        
        <div class="col-md-6 py-2">
            <div class="shadow p-3 bg-white rounded" style="height: 467.09px;">
                <h3 class="red">@lang('common.important')</h3><br>
                <p>@lang('common.messages.client.warning_one')</p>
                <p>@lang('common.messages.client.warning_two')</p>
                <p>@lang('common.messages.client.warning_three')</p>
            </div>
        </div>
        <div class="col-md-6 py-2">
            <div class="shadow p-3 bg-white rounded">
                <div class="row">
                    <div class="col-md-12">
                        <h3>@lang('common.welcome')</h3>
                    </div>
                </div><br>
                @component('components.form', [
                    "item" => null,
                    "resource" => "clients",                    
                    "route" => route('public.raffles.link.client.store')
                ])
                    @include('raffles.client.form')                
                @endcomponent                
            </div>
        </div>
    </div>
@endsection