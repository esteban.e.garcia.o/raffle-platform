@extends('layouts.app_public')

@section('content')
    @parent
    @include('raffles.info_raffle', ['raffle' => $raffle])
    <div class="row">        
        <div class="col-md-12 py-2">
            <div class="shadow p-3 bg-white rounded">
                <h3>@lang('common.hello_again', ['name' => $client->names])</h3><br>
                <div class="row">
                    <div class="col-md-10">
                        <p class="blue">@lang('common.messages.client.info_ticket')</p>
                    </div>
                    <div class="col-md-2">
                        <a class="btn btn-outline-dark disabled btn-tickets-left"><i class="fas fa-long-arrow-alt-left"></i></a>
                        <a class="btn btn-outline-dark btn-tickets-right"><i class="fas fa-long-arrow-alt-right"></i></a>
                        <a class="btn btn-outline-primary disabled btn-ticket-send">@lang('common.send')</a>
                    </div>
                </div>
                <div class="row" id="content-raffle-tickets">
                </div> 
            </div>
        </div>
    </div>
@endsection
@push('metas')
    <meta name="tickets" content="{{ json_encode($tickets) }}">
    <meta name="url-raffle-link-client-ticket" content="{{ route('public.raffles.link.client.ticket.save', ['id' => $raffle->id, 'link_id' => $link->id, 'client_id' => $client->id]) }}">
@endpush
@push('scripts')
    <script src="{{ asset('js/build/clients/index.js') }}" defer></script>   
@endpush