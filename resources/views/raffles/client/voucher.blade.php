@extends('layouts.app_public')

@section('content')
    @parent
    @include('raffles.info_raffle', ['raffle' => $raffle])
    <div class="row">        
        <div class="col-md-12 py-2">
            <div class="shadow p-3 bg-white rounded">
                <h3>@lang('common.hello_again', ['name' => __('common.messages.client.here_download_voucher')])</h3><br>
                <div class="row">
                    <div class="col-md-6">
                        <p>@lang('common.inputs.full_name'): <b>{{ $client->names . ' ' . $client->surnames }}</b></p>
                        <p>@lang('common.inputs.phone'): <b>{{ $client->phone }}</b></p>
                        <p>@lang('common.inputs.email'): <b>{{ $client->email }}</b></p>
                        <p>@lang('common.inputs.address'): <b>{{ $client->address }}</b></p>
                        <p><b>@lang('common.messages.client.remaining_lottery'): <a href="https://www.facebook.com/TheBangoO" target="_blank">@lang('common.the_bango')</a></b></p>
                        
                    </div>
                    <div class="col-md-6">                        
                        <div class="row">
                            <div class="col-md-4"></div>
                            <div class="col-md-4 box-ticket">
                                <p>@lang('common.messages.client.your_ticket_is')</p><b style="font-size: 30px;">{{ $client->ticket }}</b>
                            </div>
                            <div class="col-md-4"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-4"></div>
                            <div class="col-md-4">
                                <a href="{{ route('public.raffles.client.ticket.voucher.generate.download', ['id' => $raffle->id, 'client_id' => $client->id]) }}" target="_blank" class="btn btn-primary">@lang('common.download') @lang('common.voucher')</a>
                            </div>
                            <div class="col-md-4"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('metas')
    
@endpush
@push('scripts')
    
@endpush