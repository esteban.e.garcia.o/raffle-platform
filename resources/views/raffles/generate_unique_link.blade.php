@extends('layouts.app')

@section('content')
    @parent
    <x-content>
        <x-slot name="title">@lang('common.generate_unique_link')</x-slot>
        <div class="row py-4">
            @include('common.input', [
                "id" => "code",
                "required" => true,
                "clas" => "col-md-6",
                "label" => __('common.inputs.code'),
                "value" => "",
                "maxLength" => "5"
            ])
            <div class="col-md-6">
                <div class="form-group">                    
                    <label for="select-seller">@lang('common.select_seller')</label>   
                    <select class="form-control" id="select-seller"></select>
                </div>
            </div>
            <div class="col-md-12">
                <a href="{{ route('home') }}" class="btn btn-outline-dark"><i class="fas fa-long-arrow-alt-left"></i> @lang('common.cancel')</a>
                <a class="btn btn-outline-primary disabled" id="btn-generate-unique-link"><i class="fas fa-plus"></i> @lang('common.generate')</a>                                
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <p class="red">@lang('common.messages.reminder_link')</p>
            </div>            
            <div class="col-md-12" style="display: none;" id="label-generate-unique-link">  
                <a class="btn btn-outline-secondary mb-1" id="btn-copy-text">@lang('common.copy')</a>                 
                <textarea name="copy" id="copy" style="width: 100%; height: 260px; background: #f3f3f3"></textarea> 
            </div>
        </div>
    </x-content>
@endsection
@push('metas')
    <meta name="url-raffles-generate-unique-link" content="{{ route('api.raffles.generate.unique.link.client', ':code') }}">
    <meta name="url-raffles-sellers" content="{{ route('api.raffles.sellers', ':code') }}">    
@endpush
@push('scripts')
    <script src="{{ asset('js/build/raffles/index.js') }}" defer></script>   
@endpush