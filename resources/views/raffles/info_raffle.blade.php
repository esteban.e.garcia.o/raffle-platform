<div class="row">        
    <div class="col-md-12 py-2">
        <div class="shadow p-3 bg-white rounded">
            <div class="row">
                <div class="col-md-6">
                    <h4><a href="{{ route('public.raffles.show', ['id' => $raffle->id]) }}" @if(!isset($same)) target="_blank" @endif>{{ $raffle->name }}</a></h4>
                </div>
                <div class="col-md-6">
                    <h4>@lang('common.date_raffle_estimated', ['date' => $raffle->closed_at->format('d/m/Y')])</h4>
                </div>
            </div>                                
        </div>
    </div>
</div>