@extends('layouts.app')

@section('content')
    @parent    
    <div class="row">        
        <div class="col-md-12 py-2">
            <div class="shadow p-3 bg-white rounded" >                                                
                <div class="row">
                    <div class="col-md-12">
                        <a class="btn btn-outline-primary float-right btn-start-lottery"><i class="fas fa-gift"></i> @lang('common.lottery')</a>
                        <a style="display: none;" class="btn btn-outline-primary float-right btn-stir-lottery"><i class="fas fa-gift"></i> @lang('common.stir')</a>
                        <h3>{{ $raffle->name }}</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8" style="text-align: center;">
                        <div class="circle" id="panel-circle">5</div>
                    </div>
                    <div class="col-md-2"></div>
                </div>
                <div class="row" style="display: none; text-align: center;" id="panel-winners-list">
                    @foreach ($winners as $winner)
                        <div class="col-md-4">
                            <div class="card-winner">?</div>
                        </div>    
                    @endforeach
                </div>
                <br><br><br>
                <div class="row" style="display: none; text-align: center;" id="panel-winners-information">                    
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <h2>@lang('common.info_winner')</h2><br>
                        <h4 id="winner-name"></h4>
                        <h4 id="winner-phone"></h4>
                    </div>    
                    <div class="col-md-4"></div>                    
                </div>
            </div>
        </div>
    </div>
@endsection
@push('metas')
    <meta name="winners" content="{{ json_encode($winners) }}">
    <meta name="url-raffle-winner-update" content="{{ route('api.raffles.winner.update', ['id' => $raffle->id, 'winning_client_id' => ':winning_client_id']) }}">    
@endpush
@push('scripts')
    <script src="{{ asset('js/build/raffles/lottery.js') }}" defer></script>   
@endpush