@extends('layouts.app_public')

@section('content')
    @parent
    @include('raffles.info_raffle', ['raffle' => $raffle, 'same' => true])
    <div class="row">        
        <div class="col-md-12 py-2">
            <div class="shadow p-3 bg-white rounded">                
                @include('components.carousel', ['images' => $raffle->images])
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <p>@lang('common.inputs.description'):</p>
                        <b>{!! $raffle->description !!}</b>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p><b>@lang('common.contact_platform')</b></p>                
                        <p>@lang('common.inputs.phone'): <b>@lang('common.phone_platform')</b></p>
                        <p>@lang('common.inputs.email'): <b>@lang('common.email_platform')</b></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection