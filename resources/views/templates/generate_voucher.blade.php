<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <style>
            body {
                font-family: "Nunito", sans-serif;
            }
            .col {                
                width: 100%;                
            }
            td {
                text-align: center;
            }
            .white {
                color: white;
            }
            .box-ticket {                
                padding: 1rem;
                margin: 1rem;
                text-align: center;
            }
            footer {
                position: fixed; 
                bottom: -60px; 
                left: 0px; 
                right: 0px;
                height: 50px;                 
                text-align: center;
                line-height: 35px;
            }
        </style>
    </head>
    <body>
        <h1>{{ $title }} - @lang('common.voucher')</h1>
        <div class="row">
            <div class="col">
                <h3>{{ $raffle->name }}</h3>
                <p>@lang('common.inputs.full_name'): <b>{{ $client->names . ' ' . $client->surnames }}</b></p>
                <p>@lang('common.inputs.phone'): <b>{{ $client->phone }}</b></p>
                <p>@lang('common.inputs.email'): <b>{{ $client->email }}</b></p>
                <p>@lang('common.inputs.address'): <b>{{ $client->address }}</b></p>                
                <p>@lang('common.date_raffle_estimated', ['date' => '<b>' . $raffle->closed_at->format('d/m/Y') . '</b>'])</p>
                <p><b>@lang('common.messages.client.remaining_lottery'): <a href="https://www.facebook.com/TheBangoO" target="_blank">@lang('common.the_bango')</a></b></p>
            </div>
            <div class="col">
                <table style="width: 100%">
                    <tbody>
                        <tr>
                            <td class="white">text</td>
                            <td>
                                <div class="box-ticket">
                                    <p>@lang('common.messages.client.your_ticket_is')</p><b style="font-size: 150px;">{{ $client->ticket }}</b>
                                </div>
                            </td>
                            <td class="white">text</td>
                        </tr>
                    </tbody>
                </table>
            </div><br>            
            <div class="col">
                <p><b>@lang('common.contact_platform')</b></p>                
                <p>@lang('common.inputs.phone'): <b>@lang('common.phone_platform')</b></p>
                <p>@lang('common.inputs.email'): <b>@lang('common.email_platform')</b></p>
            </div>            
        </div>
        <footer>
            <div class="col">
                <i style="font-size: 10px">@lang('common.digital_signature'): <b>{{ $signature }}</b></i>
            </div>
        </footer>
    </body>    
</html>