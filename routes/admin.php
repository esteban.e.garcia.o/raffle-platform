<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['auth']], function () {    

    Route::resource('sellers', 'Admin\SellerController');

    Route::resource('raffles', 'Admin\RaffleController');
    Route::get('raffle/{id}/clients', 'Admin\ClientController@index')->name('raffle.clients.index');
    Route::get('raffle/{id}/sellers', 'Admin\RaffleController@sellersIndex')->name('raffle.sellers.index');
    Route::post('raffle/{id}/upload/image', 'Admin\RaffleController@uploadImage')->name('raffle.upload.image');
    Route::get('raffle/{id}/clients/returns', 'Admin\ClientController@returns')->name('raffle.clients.returns');
});