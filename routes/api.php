<?php
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => ['auth:api']], function () { 
    Route::get('sellers/datatables', 'Api\SellerController@datatables')->name('sellers.datatables');
    Route::post('raffle/{id}/seller/{seller_id}/paid_out/update', 'Api\SellerController@paidOutUpdate')->name('raffle.seller.paid_out.update');    
    Route::get('raffles/datatables', 'Api\RaffleController@datatables')->name('raffles.datatables');
    //Raffles generate
    Route::post('raffles/{code}/generate/unique/link/client', 'Api\RaffleController@generateUniqueLink')->name('raffles.generate.unique.link.client');
    Route::get('raffles/{id}/clients/datatables', 'Api\ClientController@datatables')->name('raffles.clients.datatables');
    Route::post('raffles/{id}/update/winner/{winning_client_id}', 'Api\RaffleController@updateWinner')->name('raffles.winner.update');
    Route::post('raffles/{id}/sellers/{seller_id}/{action}', 'Api\RaffleController@addOrDeleteSeller')->name('raffles.seller.add.or.delete');
    Route::post('raffles/{code}/sellers', 'Api\RaffleController@sellers')->name('raffles.sellers');
    Route::get('raffles/{id}/clients/returns/datatables', 'Api\ClientController@returnsDatatables')->name('raffles.clients.returns.datatables');
    Route::post('raffles/{id}/client/{client_id}/return/ticket', 'Api\ClientController@returnTicket')->name('raffles.client.return.ticket');
});