<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Get links uniques
Route::get('raffles/unique/generate/link/{id}/token/{token}/for/client', 'RaffleController@getLinkUniqueClient')->name('raffles.unique.generate.link.client.token');
Route::post('raffles/link/client/store', 'ClientController@store')->name('raffles.link.client.store');
Route::post('raffles/{id}/link/{link_id}/client/{client_id}/ticket/save', 'ClientController@ticketSave')->name('raffles.link.client.ticket.save');
Route::get('raffles/{id}/client/{client_id}/voucher/generate/download', 'ClientController@generateVoucherDownload')->name('raffles.client.ticket.voucher.generate.download');
Route::get('raffles/{id}', 'RaffleController@show')->name('raffles.show');