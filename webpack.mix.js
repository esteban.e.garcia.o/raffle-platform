const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js/build')
    .sass('resources/sass/app.scss', 'public/css');

mix.js('resources/js/common/table.js', 'public/js/build/common/table.js');
mix.js('resources/js/raffles/index.js', 'public/js/build/raffles/index.js');
mix.js('resources/js/clients/index.js', 'public/js/build/clients/index.js');
mix.js('resources/js/raffles/lottery.js', 'public/js/build/raffles/lottery.js');
mix.js('resources/js/admin/raffles/sellers.js', 'public/js/build/admin/raffles/sellers.js');